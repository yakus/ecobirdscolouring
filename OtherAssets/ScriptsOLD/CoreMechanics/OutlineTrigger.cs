﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineTrigger : MonoBehaviour
{
    public int m_ColorGroup = 0;
    [SerializeField]
    private bool m_IsActive = true;
    public SpriteRenderer m_ColorCutout;
    
    public bool GetIsActive()
    {
        return m_IsActive;
    }

    public void SetIsActive(bool isActive)
    {
        m_IsActive = isActive;
    }
}
