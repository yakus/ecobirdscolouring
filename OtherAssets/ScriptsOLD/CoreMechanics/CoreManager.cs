﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreManager : MonoBehaviour
{
    public Scrollbar m_VerticalScrollBar;

    // Variable holding the current color
    public int m_colorCode = 0;

    //List of all color groups
    public List<GameObject> m_ColorGroups = new List<GameObject>();

    private CutoutManager m_CutoutGroupManager;

    private void Start()
    {
        m_VerticalScrollBar.value = 0;
    }

    // Method responsible for choosing a color
    public void SelectColor(int color)
    {
        m_colorCode = color;
    }

    // Method that shows which colour group is to be colored based on selection
    public void ShowOutlinesToBeColored(int color)
    {
        color--;
        for (int i = 0; i < m_ColorGroups.Count; i++)
        {
            if (color == i)
            {
                m_CutoutGroupManager = m_ColorGroups[i].GetComponent<CutoutManager>();
                m_CutoutGroupManager.ToggleAllWhiteCutout();
            }
        }
    }
}
