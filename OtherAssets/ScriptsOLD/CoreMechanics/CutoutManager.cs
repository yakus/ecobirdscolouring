﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutoutManager : MonoBehaviour
{
    public List<SpriteRenderer> m_Colour;
    public List<SpriteRenderer> m_Outline;
    public List<SpriteRenderer> m_White;

    private void Start()
    {
        foreach (SpriteRenderer cutout in m_Colour) { cutout.gameObject.SetActive(false); }
        foreach (SpriteRenderer cutout in m_Outline) { cutout.gameObject.SetActive(true); }
        foreach (SpriteRenderer cutout in m_White) { cutout.gameObject.SetActive(true); }
    }

    public void ToggleColorCutout(GameObject theCutout)
    {
        foreach (SpriteRenderer cutout in m_Colour)
        {
            // This doesnt work because the outline clickable cutout needs to be linked to its colour respective partner

            print("Cutout in list: " + cutout.name);
            print("Clicked cutout: " + theCutout.name);

            //if (cutout.gameObject == theCutout)
            //{
            //    cutout.gameObject.SetActive(!cutout.gameObject.activeSelf);
            //}
        }
    }

    public void ToggleOutlineCutout(GameObject theCutout)
    {
        foreach (SpriteRenderer cutout in m_Outline)
        {
            if (cutout.gameObject == theCutout)
            {
                cutout.gameObject.SetActive(!cutout.gameObject.activeSelf);
            }
        }
    }

    public void ToggleWhiteCutout(GameObject theCutout)
    {
        foreach (SpriteRenderer cutout in m_White)
        {
            if (cutout.gameObject == theCutout)
            {
                cutout.gameObject.SetActive(!cutout.gameObject.activeSelf);
            }
        }
    }

    public void ToggleAllWhiteCutout()
    {
        foreach (SpriteRenderer cutout in m_White)
        {
            cutout.gameObject.SetActive(!cutout.gameObject.activeSelf);
        }
    }
}
