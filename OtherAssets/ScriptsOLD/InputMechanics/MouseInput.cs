﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInput : MonoBehaviour
{

    public CoreManager m_CoreManager;

    private void Start()
    {
        m_CoreManager = GetComponent<CoreManager>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            if (hit.collider != null && hit.collider.tag == "Outline")
            {
                print(hit.collider.name + " was clicked");
                OutlineTrigger outline = GetOutlineFromClick(hit.collider.gameObject);
                
                if (ComparePickedColorAndSelectedCutout(outline.m_ColorGroup) && outline.GetIsActive())
                {
                    outline.m_ColorCutout.gameObject.SetActive(true);
                    outline.SetIsActive(false);
                }
            }
        }
    }


    private OutlineTrigger GetOutlineFromClick(GameObject clickedOutline)
    {
        return clickedOutline.GetComponent<OutlineTrigger>();
    }

    private bool ComparePickedColorAndSelectedCutout(int OutlinesColor)
    {
        return OutlinesColor == m_CoreManager.m_colorCode;
    }
}
