﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    private Color _currColor;
    [SerializeField]
    private Scrollbar _colorBar;

    void Start()
    {
        _colorBar.value = 0;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
        mousePos.z = -5;
        if (Input.GetMouseButtonDown(0))
        {
            RayCaster(mousePos);
        }

    }

    public void GetColorFromButton(Image img)
    {
        _currColor = img.color;
    }

    private void RayCaster(Vector2 inputPos)
    {
        //print("CouchCasting...");
        RaycastHit2D hit = Physics2D.Raycast(inputPos, Vector2.zero);

        if (hit.collider == null)
        {
            print("No hit!");
            return;
        }

        if (hit.collider.tag == "ColoringPiece")
        {
            Color correct = hit.collider.GetComponent<ColourPieceProperties>()._correctColor;
            if (correct.r == _currColor.r && correct.g == _currColor.g && correct.b == _currColor.b)
            {
                print("We got a MATCH!");
                SpriteRenderer sr = hit.collider.GetComponent<SpriteRenderer>();
                sr.color = _currColor;
            } 
            else
            {
                print("The colours dont match: ");
                print("Piece: " + hit.collider.GetComponent<ColourPieceProperties>()._correctColor);
                print("Picked: " + _currColor);
            }
        }
    }
}
