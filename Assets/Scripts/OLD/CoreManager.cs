﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    private Scrollbar m_ColorListScrollbar;
    [SerializeField]
    private int m_ColorFromButton;

    [Header("Member Variables")]
    [SerializeField]
    private GameObject m_ColorGroupsParent;

    public int GetColorFromButton()
    {
        return m_ColorFromButton;
    }

    private List<ArrayList> m_ColorGroup = new List<ArrayList>();

    private void Start()
    {
        m_ColorListScrollbar.value = 0;

        foreach (Transform colorGroup in m_ColorGroupsParent.transform)
        {

            ArrayList outlines = new ArrayList();
            foreach (Transform outline in colorGroup.GetChild(1))
            {
                outlines.Add(outline);
            }
            m_ColorGroup.Add(outlines);
        }
    }

    // This button needs to disable the white fill of the correct color group once it is pressed
    public void SetUserColorFromButton(int color)
    {
        m_ColorFromButton = color;
        color--;

        for (int i = 0; i < m_ColorGroup.Count; i++)
        {
            if (color == i)
            {
                foreach (Transform outline in m_ColorGroup[i])
                {
                    CutoutInteractionManager cim = outline.GetComponent<CutoutInteractionManager>();
                    cim.enabled = true;
                    cim.DisableWhiteCutout();
                }
            } else
            {
                foreach (Transform outline in m_ColorGroup[i])
                {
                    CutoutInteractionManager cim = outline.GetComponent<CutoutInteractionManager>();
                    cim.enabled = false;
                    if (cim.IsColored() == false)
                        cim.EnableWhiteCutout();
                }
            }
        }
    }

}
