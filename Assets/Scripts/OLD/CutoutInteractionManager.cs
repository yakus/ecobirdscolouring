﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutoutInteractionManager : MonoBehaviour
{
    [Header("Scripts References")]
    [SerializeField]
    private CoreManager m_CoreMgr;

    [Header("ColorGroup")]
    [SerializeField]
    private int m_ColorGroup = 0;

    [Header("Related Cutouts")]
    [SerializeField]
    private GameObject m_Color;
    [SerializeField]
    private GameObject m_White;

    [SerializeField]
    private bool m_IsColored = false;

    private void Start()
    {
        m_Color.SetActive(false);
    }
    
    public bool IsColored()
    {
        return m_IsColored;
    }

    public void SetAsColored()
    {
        m_IsColored = true;
    }

    private void Update()
    {
        if (m_IsColored == true)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            print("Mouse Clicked!");
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            if (hit.collider != null && hit.collider.tag == "Outline")
            {
                print(hit.collider.name + " was clicked");

                CutoutInteractionManager cim = hit.collider.GetComponent<CutoutInteractionManager>();
                if (m_CoreMgr.GetColorFromButton() != 0 && m_CoreMgr.GetColorFromButton() == cim.m_ColorGroup)
                {
                    cim.m_Color.SetActive(true);
                    cim.m_IsColored = true;
                }
            }
        }
    }

    public void DisableWhiteCutout ()
    {
        m_White.SetActive(false);
    }

    public void EnableWhiteCutout()
    {
        m_White.SetActive(true);
    }

}
