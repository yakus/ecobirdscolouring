﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMoveAndZoom : MonoBehaviour
{
    Vector3 m_TouchStart;
    public float m_ZoomMin = 1f;
    public float m_ZoomMax = 5f;

    public Text m_XText;
    public Text m_YText;

    private int m_MousePosX;
    private int m_MousePosY;


    // Start is called before the first frame update
    void Start()
    {
        Camera.main.transform.position = new Vector3(0, 0, -10);
    }

    // Update is called once per frame
    void Update()
    {
        m_XText.text = "X: " + (int)Input.mousePosition.x;
        m_YText.text = "Y: " + (int)Input.mousePosition.y;

# if UNITY_EDITOR
        if (Input.mousePosition.y > 150)
# elif UNITY_ANDROID && !UNITY_EDITOR
        if (Input.mousePosition.y > 330) 
# endif
        {

            if (Input.GetMouseButtonDown(0))
            {
                m_TouchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }

            if (Input.touchCount == 2)
            {
                Touch touchOne = Input.GetTouch(0);
                Touch touchTwo = Input.GetTouch(1);

                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                Vector2 touchTwoPrevPos = touchTwo.position - touchTwo.deltaPosition;

                float prevMagnitude = (touchOnePrevPos - touchTwoPrevPos).magnitude;
                float currMagnitude = (touchOne.position - touchTwo.position).magnitude;

                float difference = currMagnitude - prevMagnitude;

                Zoom(difference * 0.01f);
            }
            else if (Input.GetMouseButton(0))
            {
                Vector3 direction = m_TouchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Camera.main.transform.position += direction;
            }
        }
    }

    void Zoom(float incrmnt)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - incrmnt, m_ZoomMin, m_ZoomMax);
    }
}
